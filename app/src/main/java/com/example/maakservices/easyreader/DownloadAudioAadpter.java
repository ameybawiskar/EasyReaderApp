package com.example.maakservices.easyreader;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amey on 13-03-2018.
 */

public class DownloadAudioAadpter extends BaseAdapter {
    List<DownloadAudioModel> downloadAudioModels;
    Context context;
    LayoutInflater inflater;


    public DownloadAudioAadpter(Context context, List<DownloadAudioModel> downloadAudioModels) {
        this.downloadAudioModels = downloadAudioModels;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public int getCount() {
        return downloadAudioModels.size();
    }

    public DownloadAudioModel getItem(int position) {
        return downloadAudioModels.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();

            convertView = this.inflater.inflate(R.layout.download_audio_list, parent, false);
            holder.videoTitle = (TextView) convertView.findViewById(R.id.textView2);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DownloadAudioModel downloadAudioModel = downloadAudioModels.get(position);
        holder.videoTitle.setText(downloadAudioModel.getAudioName());




        return convertView;
    }

    private class ViewHolder {
        private TextView videoTitle;
    }
}