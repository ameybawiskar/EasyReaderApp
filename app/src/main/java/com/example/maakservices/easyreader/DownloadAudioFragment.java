package com.example.maakservices.easyreader;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amey on 10-03-2018.
 */

public class DownloadAudioFragment extends android.support.v4.app.Fragment {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.downloadaudio_fragment, container, false);

//        recyclerView = (RecyclerView) rootView.findViewById(R.id.downloadAudioRecyclerView);

//        System.out.println("downloadAudioArrayList---------- "+downloadAudioArrayList.size());
//        downloadAudioAadpter = new DownloadAudioAadpter(downloadAudioArrayList);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(downloadAudioAadpter);

        return rootView;
    }
}
