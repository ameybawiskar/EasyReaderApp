package com.example.maakservices.easyreader;

import android.net.Uri;

/**
 * Created by Amey on 13-03-2018.
 */

public class DownloadAudioModel {
    String audioName;
    Uri audioUri;

    public DownloadAudioModel(String audioName, Uri audioUri) {
        this.audioName = audioName;
        this.audioUri = audioUri;
    }

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public Uri getAudioUri() {
        return audioUri;
    }

    public void setAudioUri(Uri audioUri) {
        this.audioUri = audioUri;
    }
}
