package com.example.maakservices.easyreader;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TextToSpeech.OnInitListener {

    final static int PERMISSION_ALL = 1;
    final static String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO};
    private TextView fileNameTextView;
    private ArrayList<String> docPaths;
    private EditText inputFromUserEditText;
    private TextToSpeech tts;
    private PDFView pdfView;
    private ProgressDialog createAudioFileProgressDialog;
    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= 23 && !isPermissionGranted()) {
            requestPermissions(PERMISSIONS, PERMISSION_ALL);
        }

        inputFromUserEditText = (EditText) findViewById(R.id.inputEditText);
        fileNameTextView = (TextView) findViewById(R.id.fileNameTextView);
        handler = new Handler();
        pdfView = findViewById(R.id.pdfView);
        fileNameTextView.setVisibility(View.GONE);
        tts = new TextToSpeech(this, this);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;

        if (id == R.id.nav_download) {
//            fragment = new DownloadAudioFragment();
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.content_main, fragment);
//            ft.commit();
            Intent intent = new Intent(MainActivity.this, AudioDownloadActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.search_file:
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setActivityTheme(R.style.AppTheme)
                            .pickFile(MainActivity.this);

                    return true;
                case R.id.play_audio:

                    speakOut();
                    // item.setIcon(R.drawable.ic_pause_circle_filled_black_24dp);

                    return true;

                case R.id.pause_audio:
                    speakStop();
                    return true;
                case R.id.audio_download:
                    System.out.println("fileNameTextView.getText().toString().length()----" + fileNameTextView.getText().toString().length());
                    if (inputFromUserEditText.getText().toString().length() > 0) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Do you want to Create audio File ?");
                        builder.setPositiveButton("Create ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DownloadAudio(fileNameTextView.getText().toString());
                            }
                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.create().show();
                    } else {
                        Toast.makeText(MainActivity.this, "Please Select File", Toast.LENGTH_LONG).show();
                    }


                    return true;
                case R.id.audio_language:

                    return true;
            }
            return false;
        }
    };


    private void DownloadAudio(String AudioFileName) {

        System.out.println("AudioFileName-------------- " + AudioFileName);
        createAudioFileProgressDialog = new ProgressDialog(this);
        createAudioFileProgressDialog.setCancelable(true);
        createAudioFileProgressDialog.setMessage("Creating Audio File...");
        createAudioFileProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        createAudioFileProgressDialog.setProgress(0);
        createAudioFileProgressDialog.setMax(10);
        createAudioFileProgressDialog.show();

        final int totalProgressTime = 100;
//        final Thread t = new Thread() {
//            @Override
//            public void run() {
//                int jumpTime = 0;
//
//                while (jumpTime < totalProgressTime) {
//                    try {
//                        sleep(200);
//                        jumpTime += 5;
//                        createAudioFileProgressDialog.incrementProgressBy(1);
//                    } catch (InterruptedException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                }
//                            }
//        };
//        t.start();


        File dir = new File(Environment.getExternalStorageDirectory(), "/" + "EasyReader");
        String directory = dir.getPath();
        System.out.println("Directory created " + directory);

        try {
            if (dir.mkdirs()) {
                System.out.println("Directory  created");
            } else {
                System.out.println("Directory not created");
            }
        } catch (Exception e) {
            System.out.println("directory creation EXCEPTION" + e.getMessage());
        }


        new Thread(new Runnable() {
            public void run() {
                try {
                    int jumpTime = 0;

                    while (jumpTime < totalProgressTime) {
                        try {
                            Thread.sleep(100);
                            jumpTime += 5;
                            createAudioFileProgressDialog.incrementProgressBy(1);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                } // Just catch the InterruptedException

                // Now we use the Handler to post back to the main thread
                handler.post(new Runnable() {
                    public void run() {
                        // Set the View's visibility back onpbar.setVisibility(View.INVISIBLE);
                        createAudioFileProgressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Audio file cretaed sucessfully check in file explore", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }).start();


        if (AudioFileName.isEmpty()) {
            System.out.println("yes its empty");
            Random rand = new Random();

            AudioFileName = "EasyReaderAudio" + rand.nextInt(100) + ".wav";
        } else {
            System.out.println("No its not empty");
        }
        String audioFilenameWithoutExtension = AudioFileName.substring(0, AudioFileName.lastIndexOf("."));
        String audioFileWithExtension = audioFilenameWithoutExtension.replaceAll("\\s", "");

        HashMap<String, String> myHashRender = new HashMap();
        String textToConvert = inputFromUserEditText.getText().toString();
        String destinationFileName = directory + "/" + audioFileWithExtension + ".wav";
        System.out.println("destinationFileName------------  " + destinationFileName);
        myHashRender.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, textToConvert);
        tts.synthesizeToFile(textToConvert, myHashRender, destinationFileName);
        System.out.println("Test Complete");


    }


    @TargetApi(Build.VERSION_CODES.N_MR1)
    private boolean isPermissionGranted() {
        if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                || this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                || this.checkSelfPermission(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED
                || this.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                || this.checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {


            return true;
        } else {

            return false;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                }
                break;
        }
        addThemToView(docPaths);
    }

    private void addThemToView(ArrayList<String> docPaths) {

        ArrayList<String> filePaths = new ArrayList<>();
        String getDocumentPath = null;

        if (docPaths != null) filePaths.addAll(docPaths);

        System.out.println("filePaths----------------  " + filePaths);
        for (int forloopforgetfilename = 0; forloopforgetfilename < filePaths.size(); forloopforgetfilename++) {
            getDocumentPath = filePaths.get(forloopforgetfilename);
        }


        String selectedDocumentName = getDocumentPath.substring(getDocumentPath.lastIndexOf('/') + 1);

        fileNameTextView.setVisibility(View.VISIBLE);
        fileNameTextView.setText(selectedDocumentName);

        if (selectedDocumentName.contains(".pdf")) {


            String parsedText = "";
            System.out.println("---------------------1");
            PdfReader reader = null;
            try {
                reader = new PdfReader(getDocumentPath);

            } catch (IOException e) {
                System.out.println("Exception ----- while Selecting document path not found exception ");
                e.printStackTrace();
            }
            System.out.println("---------------------2");
            int n = reader.getNumberOfPages();
            System.out.println("---------------------3");
            for (int i = 0; i < n; i++) {
                System.out.println("asasas ");
                try {
                    parsedText = parsedText + PdfTextExtractor.getTextFromPage(reader, i + 1).trim() + "\n"; //Extracting the content from the different pages
                } catch (IOException e) {
                    System.out.println("Exception ----- while parsing Pdf file ");
                    e.printStackTrace();
                }
            }
            System.out.println("parsedText" + parsedText);
            reader.close();

            inputFromUserEditText.setText(parsedText);
            try {
                System.out.println("getDocumentPath---- " + getDocumentPath);

                Uri myUri = Uri.parse("file://" + getDocumentPath);
                System.out.println("myUri-----------  " + myUri);
                pdfView.fromUri(myUri).enableSwipe(true) // allows to block changing pages using swipe
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0).spacing(0)
                        .load();
            } catch (Exception e) {
                System.out.println(" Exception --- " + e.getMessage());
            }
        } else if (selectedDocumentName.contains(".txt")) {
            System.out.println("Txt File is here");
            System.out.println("File path----" + getDocumentPath);
            File file = new File(getDocumentPath);
            System.out.println("file obejct ------" + file);

            //Read text from file
            StringBuilder text = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
            } catch (IOException e) {
                //You'll need to add proper error handling here
                System.out.println("Exception while acccepting text file ");
            }

            System.out.println("Check ---- Text from Txt  File----- " + text.toString());
            inputFromUserEditText.setText(text.toString());

        }
    }


    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }


    private void speakOut() {

        String text = inputFromUserEditText.getText().toString();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);

    }

    private void speakStop() {
        tts.stop();
    }


    //public class CreateAuidoFileAsynTask extends AsyncTask<Void, Void, Void>

}
