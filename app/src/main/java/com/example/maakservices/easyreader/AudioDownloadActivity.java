package com.example.maakservices.easyreader;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Amey on 13-03-2018.
 */

public class AudioDownloadActivity extends AppCompatActivity {
    private ListView downloadAudioListView;

    private ArrayList<DownloadAudioModel> downloadAudioArrayList;
    private DownloadAudioAadpter downloadAudioAadpter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ccheck);
        downloadAudioListView = (ListView) findViewById(R.id.listzcx);

        downloadAudioArrayList = new ArrayList<>();
        String path = Environment.getExternalStorageDirectory().toString() + "/EasyReader";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        Log.d("Files", "Size: " + files.length);
        for (int i = 0; i < files.length; i++) {
            Log.d("Files", "FileName:" + files[i].getName());
            Uri downloadedAudioUri = Uri.parse("file://" + files[i].getAbsolutePath());
            downloadAudioArrayList.add(new DownloadAudioModel(files[i].getName(), downloadedAudioUri));
        }
        downloadAudioAadpter = new DownloadAudioAadpter(AudioDownloadActivity.this, downloadAudioArrayList);
        downloadAudioListView.setAdapter(downloadAudioAadpter);

        downloadAudioListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DownloadAudioModel selectedDownloadAudio = downloadAudioArrayList.get(position);
                System.out.println("selectedDownloadAudio.getAudioUri()" + selectedDownloadAudio.getAudioUri());

//
                File file = new File("/storage/emulated/0/EasyReader/easytest.wav");
//                System.out.println("Uri.fromFile(file)------------- " + Uri.fromFile(file));
//                intent.setDataAndType(Uri.fromFile(file), "audio/*");
//                startActivity(intent);

                System.out.println("BuildConfig.APPLICATION_ID --------- " + BuildConfig.APPLICATION_ID);
                System.out.println("File--");
                Uri selectedFileURI = FileProvider.getUriForFile(AudioDownloadActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
                System.out.println("selectedFileURI----------- " + selectedFileURI);

                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setDataAndType(selectedFileURI, "audio/*");
                startActivity(intent);
            }
        });
    }


}
